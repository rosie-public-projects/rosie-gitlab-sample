[![rosieyohannan](https://circleci.com/circleci/JXGebBh1Qw1VJDhAYyTuTB/4FxkhrCozLnepcQGWF46P9.svg?style=svg)](https://app.circleci.com/pipelines/circleci/JXGebBh1Qw1VJDhAYyTuTB/4FxkhrCozLnepcQGWF46P9?branch=master)

What do steps look like in GitLab?

# Sample GitLab Project

[![CircleCI](https://dl.circleci.com/insights-snapshot/circleci/JXGebBh1Qw1VJDhAYyTuTB/4FxkhrCozLnepcQGWF46P9/master/say-hello-workflow/badge.svg?window=30d&circle-token=f4753d3f83e5b1cede40234c0f6c63f36ab15a9b)](https://app.circleci.com/insights/circleci/JXGebBh1Qw1VJDhAYyTuTB/4FxkhrCozLnepcQGWF46P9/workflows/say-hello-workflow/overview?branch=master&reporting-window=last-30-days&insights-snapshot=true)

This sample project shows how a project in GitLab looks for demonstration purposes. It contains issues, merge requests and Markdown files in many branches,
named and filled with lorem ipsum.

You can look around to get an idea how to structure your project and, when done, you can safely delete this project.

[Learn more about creating GitLab projects.](https://docs.gitlab.com/ee/gitlab-basics/create-project.html)
